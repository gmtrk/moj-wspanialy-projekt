#include "WyrazenieZesp.hh"
#include "LZespolona.hh"
#include <iostream>
#include <string>
using namespace std;

ostream& operator << (ostream& os, LZespolona& LZ) //strumień wyjściowy dla LZespolona
{
     return os << "(" << LZ.re << showpos << LZ.im << "i)" << noshowpos;     
     
}
istream& operator >> (istream& is, LZespolona& LZ)
{
    while (isspace(is.peek())) //jezeli znakiem jest spacja to jest ignorowana
    {
        is.ignore();
    }

    if (is.peek() != '(') //sprawdza czy pierwszy znak to nawias, jeżeli nie przechodzimy do failstate
    {
        is.setstate(ios::failbit);
        return is;
    }
    is.get(); // 'zbiera' nawias
    is >> LZ.re; 
    is >> LZ.im;

    if (is.peek() != 'i') // sprawdza czy kolejny znak to i
    {
        is.setstate(ios::failbit);
        return is;
    }
    is.get(); // 'zbiera' i
    if (is.peek() != ')')  // sprawdza czy ostatnia to nawias
    {
        is.setstate(ios::failbit);
        return is;
    }
    is.get(); // 'zbiera' nawias
    return is;
}
ostream& operator << (ostream& os, WyrazenieZesp& WyrZ) //operator wyswietlajacy wyrazenie zespolone
 { 
    switch (WyrZ.Op)
        {
        case Op_Dodaj:

            os << WyrZ.Arg1 << "+" << WyrZ.Arg2;
            break;
        case Op_Odejmij:

            os << WyrZ.Arg1 << "-" << WyrZ.Arg2;
            break;
        case Op_Mnoz:

         os << WyrZ.Arg1 << "*" << WyrZ.Arg2;
            break;
        case Op_Dziel:

            os << WyrZ.Arg1 << "/" << WyrZ.Arg2;
            break;
        default:
        cout << "Niepoprawny operator" << endl;
             break;
        }

    return os;
 }


LZespolona Oblicz(WyrazenieZesp  WyrZ) //funckja obliczająca wyrażenie zespolone
{
    LZespolona Wynik;
 switch (WyrZ.Op)
 {
 case Op_Dodaj:
        Wynik = WyrZ.Arg1 + WyrZ.Arg2;
     break;
 case Op_Odejmij:
        Wynik = WyrZ.Arg1 - WyrZ.Arg2;
     break;
 case Op_Mnoz:
        Wynik = WyrZ.Arg1 * WyrZ.Arg2;
     break;
 case Op_Dziel:
        Wynik = WyrZ.Arg1 / WyrZ.Arg2;
     break;

 default:
    cout << "Niepoprawny operator" << endl;
     break;
 }
 return Wynik;
}

/*
* Funkja porównująca dwie Liczy Zespolone
*/
int operator ==(LZespolona Z1, LZespolona Z2)
{
    if (Z1.re == Z2.re && Z1.im == Z2.im)
    {
        return 1;
    }
    else
    {
        return 0;
    }
    
    
}


/*
 * Tu nalezy zdefiniowac funkcje, ktorych zapowiedzi znajduja sie
 * w pliku naglowkowym.
 */
