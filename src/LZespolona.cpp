#include "LZespolona.hh"
#include <iostream>
#include <math.h>
#include <string>

using namespace std;

/*!
 * Realizuje dodanie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik dodawania,
 *    Skl2 - drugi skladnik dodawania.
 * Zwraca:
 *    Sume dwoch skladnikow przekazanych jako parametry.
 */
LZespolona  operator + (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona Wynik;

  Wynik.re = Skl1.re + Skl2.re;
  Wynik.im = Skl1.im + Skl2.im;
  return Wynik;
}
/*!
 * Realizuje odejmowanie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik odejmowania,
 *    Skl2 - drugi skladnik odejmnowania.
 * Zwraca:
 *    Różnice dwoch skladnikow przekazanych jako parametry.
 */
LZespolona operator - (LZespolona Skl1, LZespolona Skl2){
  LZespolona Wynik;

  Wynik.re = Skl1.re - Skl2.re;
  Wynik.im = Skl1.im - Skl2.im;
  return Wynik;
}
/*!
 * Realizuje mnożenie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik mnożenia,
 *    Skl2 - drugi skladnik mnożenia.
 * Zwraca:
 *    Iloczyn dwoch skladnikow przekazanych jako parametry.
 */
LZespolona operator * (LZespolona Skl1, LZespolona Skl2){
  LZespolona Wynik;

  Wynik.re = (Skl1.re * Skl2.re) - (Skl1.im * Skl2.im);
  Wynik.im = (Skl1.im * Skl2.re) + (Skl2.im * Skl1.re);
  return Wynik;
}
/*!
 * Realizuje dzielenie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik dzielenia,
 *    Skl2 - drugi skladnik dzielenia.
 * Argument Skl2.im jest ujemny co wynika z twierdzenia sprzężenia:
 * Sprzężenie z Skl1.re + Skl1.im = Skl1.re - Skl1.im
 * Zwraca:
 *    Iloraz dwoch skladnikow przekazanych jako parametry.
 */
LZespolona operator / (LZespolona Skl1, LZespolona Skl2){
  LZespolona Wynik;
if (pow(Skl2.re,2) + pow(Skl2.im,2) != 0){
  Wynik.re = ((Skl1.re * Skl2.re) - (Skl1.im * -Skl2.im))/(pow(Skl2.re,2) + pow(Skl2.im,2));
  Wynik.im = (Skl1.im * Skl2.re) + (-Skl2.im * Skl1.re)/(pow(Skl2.re,2) + pow(Skl2.im,2));
  return Wynik;
}
else
  {
     cout << "Blad, dzielenie przez zero" << endl;
     return Skl1;
   
  }
}

void WyswietlLz(LZespolona Z1) {



if (Z1.im>0)

  cout << "(" << Z1.re << showpos << Z1.im << "i)" << noshowpos;
   else
   cout << "(" << Z1.re << Z1.im << "i)";

return;

  

  
}
