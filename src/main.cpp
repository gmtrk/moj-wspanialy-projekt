#include <iostream>
#include <string>
#include "BazaTestu.hh"
#include "WyrazenieZesp.hh"
#include "LZespolona.hh"

using namespace std;




int main(int argc, char **argv)
{

  if (argc < 2) {
    cout << endl;
    cout << " Brak opcji okreslajacej rodzaj testu." << endl;
    cout << " Dopuszczalne nazwy to:  latwy, trudny." << endl;
    cout << endl;
    return 1;
  }


  BazaTestu   BazaT = { nullptr, 0, 0 };

  if (InicjalizujTest(&BazaT,argv[1]) == false) {
    cerr << " Inicjalizacja testu nie powiodla sie." << endl;
    return 1;
  }


  
  cout << endl;
  cout << " Start testu arytmetyki zespolonej: " << argv[1] << endl;
  cout << endl;

  WyrazenieZesp   WyrZ_PytanieTestowe;
  LZespolona odp; // wynik podany przez użytkownika
  LZespolona Wynik; // wynik operacji
  double Dobrze = 0; //liczba dobrych odpowiedzi
  double Zle = 0; //liczba złych odpowiedzi
  int fail = 3; // liczba kontrolna do niepoprawnie wprowadzonych liczb zespolonych
  bool poprawnie = false; // zmienna kontrolna do poprawnej odpowiedzi

  while (PobierzNastpnePytanie(&BazaT,&WyrZ_PytanieTestowe)) {
    cout << " Podaj wynik operacji: ";
    cout << WyrZ_PytanieTestowe << endl;
    while ((poprawnie = false) || fail >= 0)
    {
    cin >> odp;
    if (cin.fail())     //jezeli uzytkownik niepoprawnie wprowadzi liczbe zespolona
     {
       cin.clear();
       cin.ignore(1000, '\n');
       fail--;
       cout << " Niepoprawny format liczby zespolonej. " << endl;
       cout << " Sprobuj (a+bi) lub (a+bi). " << endl;
       if (fail < 0)        // zapobiega wyświetlaniu -1 liczby prób
       {
       cout << " Pozostała liczba prob: " << fail << endl;
       }
     }
     else
     {
       poprawnie = true;
     }
     
    }
    poprawnie = false;
    fail = 3; // reset liczby prób
    Wynik =  Oblicz(WyrZ_PytanieTestowe);
    cout << " Twoja odpowiedz: ";
    cout << odp << endl;
    if (Wynik == odp)
    {
      cout << " :) Odpowiedz poprawna";
      Dobrze++;
    }
    else{
      cout << " :( Blad. Prawidlowym wynikiem jest: " << Wynik << endl;
      Zle++;
    }
  }

  
  cout << endl;
  cout << " Koniec testu" << endl;
  cout << endl;

  cout << " Ilosc dobrych odpowiedzi: " << Dobrze << endl;
  cout << " Ilosc blednych odpowiedzi: " << Zle << endl;
  cout << " Wynik procentowy poprawnych odpowiedzi: " << (Dobrze/(Dobrze + Zle)) * 100 << "%" << endl;

}

/****************************************************************************************************************************************
 * Jakub Gmiterek 254007 pn 17:05 Programowanie Obiektowe                                                                               *
 * Uruchamiam program przy uzyciu ./a.out                                                                                               *
 * Program nie uruchamia sie, poniewaz nie został określony rodzaj testu.                                                               *
 * Następnie uruchamiam z flagą 'latwt' i na pierwsze trzy pytania odpowiadam poprawnie:                                                *
 * (3+3i), (1-1i), (0+9i). Na ostatnie pytanie wpisuje niepoprawny format trzy razy:                                                    *
 * (2+3), (a+ci), (5-21i i za każdym razem otrzymuje wiadomość o niepoprawnym formacie i ile zostało mi jeszcze prób.                   *
 * Mając jeszcze jedną szansę wpisuję (4-8i), co jest poprawnym formatem ale nie jest poprawnym wynikiem.                               *
 * Program jest smutny, zwraca błąd i podaje prawidłowy wynik.                                                                          *
 * Na końcu testu otrzymuje swoje statystyki: 3 dobre i jedna niepoprawna, oraz mój procentowy wynik poprawnym odpowiedzi, czyli 75%    *
 * Test przeprowadzam analogicznie dla flagi 'trudny'. Program kompiluje się i działa bez błędów                                        *
 * **************************************************************************************************************************************/