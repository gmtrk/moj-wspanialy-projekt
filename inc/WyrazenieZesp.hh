#ifndef WYRAZENIEZESP_HH
#define WYRAZENIEZESP_HH


#include <iostream>
#include "LZespolona.hh"
using namespace std;


/*!
 * Modeluje zbior operatorow arytmetycznych.
 */
enum Operator { Op_Dodaj, Op_Odejmij, Op_Mnoz, Op_Dziel };



/*
 * Modeluje pojecie dwuargumentowego wyrazenia zespolonego
 */
struct WyrazenieZesp {
  LZespolona   Arg1;   // Pierwszy argument wyrazenia arytmetycznego
  Operator     Op;     // Opertor wyrazenia arytmetycznego
  LZespolona   Arg2;   // Drugi argument wyrazenia arytmetycznego
};


/*
 * Funkcje ponizej nalezy zdefiniowac w module.
 *
 */


ostream& operator << (ostream& os, LZespolona& LZ);
ostream& operator << (ostream& os, WyrazenieZesp& WyrZ);
istream& operator >> (istream& is, LZespolona& LZ);
LZespolona Oblicz(WyrazenieZesp  WyrZ);
int operator ==(LZespolona Z1, LZespolona Z2);

#endif
